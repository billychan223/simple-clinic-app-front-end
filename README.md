# Simple clinic app

## Technical stack
* Expo
* React native

## How to start

1. Go to spin up the backend server first <https://gitlab.com/billychan223/clinic-example-using-nestjs>
2. `yarn install` to install the required package
3. This is a expo managed flow app so you can use 

```
yarn start 
```

to kickstart expo