import React from 'react';
import {StyleSheet} from 'react-native';

import {Heading} from '../components/Heading';
import {Input} from '../components/Input';
import {FilledButton} from '../components/FilledButton';
import {Error} from '../components/Error';
import {IconButton} from '../components/IconButton';
import {AuthContainer} from '../components/AuthContainer';
import {AuthContext} from '../contexts/AuthContext';
import {Loading} from '../components/Loading';

import {useForm} from 'react-hook-form';
import {HelperText} from 'react-native-paper';

export function RegistrationScreen({navigation}) {
  const {register} = React.useContext(AuthContext);
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState('');

  const {control, handleSubmit, errors} = useForm({mode: 'onSubmit'});

  return (
    <AuthContainer>
      <IconButton
        style={styles.closeIcon}
        name={'close-circle-outline'}
        onPress={() => {
          navigation.pop();
        }}
      />
      <Heading style={styles.title}>REGISTRATION</Heading>
      <Error error={error} />
      <Input
        style={styles.input}
        placeholder={'Email'}
        keyboardType={'email-address'}
        control={control}
        name={'email'}
      />
      <HelperText type="error" visible={errors?.email !== undefined}>
        Invalid email
      </HelperText>
      <Input
        style={styles.input}
        placeholder={'Password'}
        secureTextEntry
        name={'password'}
        control={control}
      />
      <HelperText type="error" visible={errors?.password !== undefined}>
        Invalid password , at least 8 characters
      </HelperText>
      <Input
        style={styles.input}
        placeholder={'name'}
        control={control}
        name={'name'}
        control={control}
      />
      <HelperText type="error" visible={errors?.name !== undefined}>
        name is required
      </HelperText>
      <Input
        style={styles.input}
        placeholder={'phone number'}
        keyboardType={'phone-pad'}
        name={'phoneNumber'}
        control={control}
      />
      <HelperText type="error" visible={errors?.phoneNumber !== undefined}>
        phone number is required
      </HelperText>
      <Input
        style={styles.input}
        placeholder={'address'}
        multiline
        numberOfLines={4}
        name={'address'}
        control={control}
      />
      <HelperText type="error" visible={errors?.address !== undefined}>
        address is required
      </HelperText>
      <FilledButton
        title={'Register'}
        style={styles.loginButton}
        onPress={handleSubmit(async val => {
          try {
            setLoading(true);
            await register(val);
            navigation.pop();
          } catch (e) {
            setError(e.message);
            setLoading(false);
          }
        })}
        type="submit"
      />
      <Loading loading={loading} />
    </AuthContainer>
  );
}

const styles = StyleSheet.create({
  title: {
    marginBottom: 48,
  },
  input: {
    marginVertical: 8,
  },
  loginButton: {
    marginVertical: 32,
  },
  closeIcon: {
    position: 'absolute',
    top: 60,
    right: 16,
  },
});
