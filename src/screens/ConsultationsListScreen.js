import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {Card} from '../components/Card';
import {HeaderIconButton} from '../components/HeaderIconButton';
import {AuthContext} from '../contexts/AuthContext';
import {Consultation} from '../components/Consultation';
import {useGet} from '../hooks/useGet';
import {HeaderIconsContainer} from '../components/HeaderIconsContainer';
import {ThemeContext} from '../contexts/ThemeContext';
import {Agenda} from 'react-native-calendars';
export function ConsultationsListScreen({navigation}) {
  const {logout} = React.useContext(AuthContext);
  const switchTheme = React.useContext(ThemeContext);
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <HeaderIconsContainer>
          <HeaderIconButton
            name={'color-palette'}
            onPress={() => {
              switchTheme();
            }}
          />
          <HeaderIconButton
            name={'log-out'}
            onPress={() => {
              logout();
            }}
          />
        </HeaderIconsContainer>
      ),
    });
  }, [navigation, logout, switchTheme]);
  const consultations = useGet('consultations');

  const consultationsByDate = consultations.reduce((groups, consultation) => {
    const date = consultation.dateTime.split('T')[0];
    if (!groups[date]) {
      groups[date] = [];
    }
    groups[date].push(consultation);
    return groups;
  }, {});

  function renderConsultation({item: consultation}) {
    return <Consultation consultation={consultation} />;
  }

  return (
    <Agenda
      items={consultationsByDate}
      renderItem={item => renderConsultation({item})}
      renderEmptyData={() => {
        return (
          <Card styles={styles.emptyDate}>
            <View>
              <Text style={styles.emptyDateText}>No Consultation</Text>
            </View>
          </Card>
        );
      }}
      rowHasChanged={(r1, r2) => {
        return r1.dateTime !== r2.dateTime;
      }}
      pastScrollRange={50}
      futureScrollRange={50}
      hideKnob={false}
    />
  );
}

const styles = StyleSheet.create({
  ConsultationsListContainer: {
    paddingVertical: 8,
    marginHorizontal: 8,
  },
  emptyDate: {
    height: '100%',
    paddingVertical: 8,
    marginVertical: 30,
  },
  emptyDateText: {
    fontSize: 22,
    fontWeight: 'bold',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
});
