import React, {useEffect} from 'react';
import {StyleSheet} from 'react-native';

import {Heading} from '../components/Heading';
import {Input} from '../components/Input';
import {FilledButton} from '../components/FilledButton';
import {TextButton} from '../components/TextButton';
import {Error} from '../components/Error';
import {AuthContainer} from '../components/AuthContainer';
import {AuthContext} from '../contexts/AuthContext';
import {Loading} from '../components/Loading';
import {useForm} from 'react-hook-form';
import {HelperText} from 'react-native-paper';
export function LoginScreen({navigation}) {
  const {login} = React.useContext(AuthContext);
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState('');
  const {control, handleSubmit, errors} = useForm({mode: 'onSubmit'});
  const [formErrors, setFormErrors] = React.useState({});

  // @Todo remove this hacky way to force re-render
  useEffect(() => {
    setFormErrors(errors);
  }, [errors]);
  return (
    <AuthContainer>
      <Heading style={styles.title}>LOGIN</Heading>
      <Error error={error} />
      <Input
        name={'email'}
        errors={errors.email}
        control={control}
        style={styles.input}
        placeholder={'Email'}
        keyboardType={'email-address'}
      />
      <HelperText type="error" visible={errors?.email !== undefined}>
        Invalid email
      </HelperText>
      <Input
        name={'password'}
        control={control}
        style={styles.input}
        placeholder={'Password'}
        secureTextEntry
      />
      <HelperText type="error" visible={errors?.password !== undefined}>
        Please enter your password
      </HelperText>
      <FilledButton
        title={'Login'}
        style={styles.loginButton}
        onPress={handleSubmit(async val => {
          try {
            setLoading(true);
            await login(val.email, val.password);
          } catch (e) {
            setError(e.message);
            setLoading(false);
          }
        })}
        type="submit"
      />
      <TextButton
        title={'Do you have an account? Create one'}
        onPress={() => {
          navigation.navigate('Registration');
        }}
      />
      <Loading loading={loading} />
    </AuthContainer>
  );
}

const styles = StyleSheet.create({
  title: {
    marginBottom: 48,
  },
  input: {
    marginVertical: 8,
  },
  loginButton: {
    marginVertical: 32,
  },
});
