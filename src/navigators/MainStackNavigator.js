import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {ConsultationsListScreen} from '../screens/ConsultationsListScreen';

const MainStack = createStackNavigator();

export function MainStackNavigator() {
  return (
    <MainStack.Navigator>
      <MainStack.Screen
        name={'ConsultationList'}
        component={ConsultationsListScreen}
        options={{
          title: 'Consultations List',
        }}
      />
    </MainStack.Navigator>
  );
}
