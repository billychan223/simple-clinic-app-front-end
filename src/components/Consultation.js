import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {
  Modal,
  Portal,
  Title,
  DataTable,
  Subheading,
  Paragraph,
  Divider,
} from 'react-native-paper';
import {Card} from './Card';

export function Consultation({consultation}) {
  const [visible, setVisible] = React.useState(false);
  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  return (
    <>
      <Portal>
        <Modal
          visible={visible}
          onDismiss={hideModal}
          contentContainerStyle={styles.modalContainer}>
          <Title>{consultation.patientName}</Title>
          <Text style={styles.date}>
            {new Date(consultation.dateTime).toUTCString()}
          </Text>
          <Text></Text>
          <Subheading>Diagnosis</Subheading>
          <Paragraph>{consultation.diagnosis}</Paragraph>
          <Subheading>Medication</Subheading>
          <Paragraph>{consultation.medication}</Paragraph>
          <Subheading style={styles.subheading}>Other Details</Subheading>
          <DataTable>
            <DataTable.Row>
              <DataTable.Cell>Consultation Fee</DataTable.Cell>
              <DataTable.Cell numeric>
                {consultation.consultationFee}
              </DataTable.Cell>
            </DataTable.Row>

            <DataTable.Row>
              <DataTable.Cell>Doctor</DataTable.Cell>
              <DataTable.Cell numeric>{consultation.doctorName}</DataTable.Cell>
            </DataTable.Row>
            <DataTable.Row>
              <DataTable.Cell>NeedToFollowup</DataTable.Cell>
              <DataTable.Cell numeric>
                {consultation.hasFollowup ? 'yes' : 'no'}
              </DataTable.Cell>
            </DataTable.Row>
          </DataTable>
          <Divider />
        </Modal>
      </Portal>
      <Card style={styles.card} onPress={showModal}>
        <View style={styles.infoContainer}>
          <Text style={styles.name}>{consultation.patientName}</Text>
          <Text style={styles.date}>
            {new Date(consultation.dateTime).toUTCString()}
          </Text>
          <Text style={styles.doctor}>{consultation.doctorName}</Text>
        </View>
      </Card>
    </>
  );
}

const styles = StyleSheet.create({
  card: {
    marginVertical: 20,
  },
  infoContainer: {
    padding: 16,
  },
  name: {
    fontSize: 22,
    fontWeight: 'bold',
  },
  date: {
    fontSize: 16,
    fontWeight: '600',
    marginBottom: 8,
  },
  doctor: {
    fontSize: 16,
    fontWeight: '400',
    color: '#787878',
  },
  modalContainer: {
    backgroundColor: 'white',
    padding: 20,
  },
  subheading: {
    marginTop: 30,
  },
});
