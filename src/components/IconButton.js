import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import {useTheme} from '@react-navigation/native';

export function IconButton({name, style, onPress}) {
  const {colors} = useTheme();
  return (
    <TouchableOpacity style={[styles.container, style]} onPress={onPress}>
      <Ionicons name={name} color={colors.primary} size={32} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {},
});
