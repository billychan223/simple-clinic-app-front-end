import React from 'react';
import {StyleSheet, TextInput} from 'react-native';
import {Controller} from 'react-hook-form';

export function Input({style, control, name, errors, ...props}) {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue=""
      render={({onChange, value}) => (
        <TextInput
          {...props}
          onChangeText={text => onChange(text)}
          value={value}
          style={[styles.input, style]}
          placeholderTextColor={'darkgray'}
          autoCapitalize={'none'}
        />
      )}
      rules={{required: true}}
    />
  );
}

const styles = StyleSheet.create({
  input: {
    backgroundColor: '#e8e8e8',
    width: '100%',
    padding: 20,
    borderRadius: 8,
    color: 'black',
  },
});
